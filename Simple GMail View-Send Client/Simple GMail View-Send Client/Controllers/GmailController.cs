﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Mvc;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Simple_GMail_View_Send_Client.Models;
using System.Text;
using System.Linq;
using System.Net.Mail;

namespace Simple_GMail_View_Send_Client.Controllers
{
    public class GmailController : Controller
    {



        private string accessToken;
        private string GetUserEmail()
        {
            return User.FindFirst(c => c.Type == ClaimTypes.Email).Value;
        }

        private async void GetToken()
        {
            accessToken = await HttpContext.GetTokenAsync("access_token");
        }

        private GmailService GetService()
        {
            GetToken();
            var credential = GoogleCredential.FromAccessToken(accessToken);
            var service = new GmailService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential
            });
            return service;
        }



        [Authorize]
        [HttpGet]
        public async Task<IActionResult> GetListEmail(string LabelId, string nameLabel)
        {
            string UserEmail = GetUserEmail();
            var service = GetService();
            List<MessageModel> listMessages = new List<MessageModel>();

            List<Message> result = new List<Message>();
            var emailListRequest = service.Users.Messages.List(UserEmail);
            emailListRequest.LabelIds = LabelId;
            emailListRequest.IncludeSpamTrash = false;
            emailListRequest.MaxResults = 1000;

            var emailListResponse = await emailListRequest.ExecuteAsync();

            if (emailListResponse != null && emailListResponse.Messages != null)
            {

                foreach (var email in emailListResponse.Messages)
                {
                    MessageModel message = new MessageModel();
                    var emailInfoRequest = service.Users.Messages.Get(UserEmail, email.Id);

                    var emailInfoResponse = await emailInfoRequest.ExecuteAsync();

                    if (emailInfoResponse != null)
                    {
                        message.ID = listMessages.Count + 1;
                        message.EmailId = email.Id;
                        foreach (var mParts in emailInfoResponse.Payload.Headers)
                        {
                            if (mParts.Name == "Date")
                            {
                                message.Date = mParts.Value;
                            }
                            else if (mParts.Name == "From")
                            {
                                message.From = mParts.Value;
                            }
                            else if (mParts.Name == "Subject")
                            {
                                message.EmailTitle = mParts.Value;
                            }
                        }
                        listMessages.Add(message);
                    }
                }
            }
            ViewBag.Message = nameLabel;
            ViewBag.UserEmail = UserEmail;
            return View("~/Views/Home/Index.cshtml", listMessages);
        }

     


        private String getNestedParts(IList<MessagePart> part, string curr)
        {
            string str = curr;
            if (part == null)
            {
                return str;
            }
            else
            {
                foreach (var parts in part)
                {
                    if (parts.Parts == null)
                    {
                        if (parts.Body != null && parts.Body.Data != null)
                        {
                            str += parts.Body.Data;
                        }
                    }
                    else
                    {
                        return getNestedParts(parts.Parts, str);
                    }
                }

                return str;
            }
        }


        public async Task<IActionResult> GetEmailContent(string EmailId)
        {
            string UserEmail = GetUserEmail();
            var service = GetService();

            var emailInfoRequest = service.Users.Messages.Get(UserEmail, EmailId);

            // Make another request for that email id...
            emailInfoRequest.Format = UsersResource.MessagesResource.GetRequest.FormatEnum.Raw;
            //    var res = emailInfoRequest.Fields.Replace("-", "+").Replace("_", "/");
            var res = emailInfoRequest.Execute();
            //      byte[] bodyBytes = Convert.FromBase64String(res);
            var body = res.Raw;

            //      string val = Encoding.UTF8.GetString(bodyBytes);

            /*
            if (emailInfoResponse != null)
            {
                // Loop through the headers and get the fields we need...
                foreach (var mParts in emailInfoResponse.Payload.Headers)
                {

                    if (emailInfoResponse.Payload.Parts == null && emailInfoResponse.Payload.Body != null)
                    {
                        body = emailInfoResponse.Payload.Body.Data;
                    }
                    else
                    {
                        body = getNestedParts(emailInfoResponse.Payload.Parts, "");
                    }
                    // Need to replace some characters as the data for the email's body is base64   */
            string codedBody = body.Replace("-", "+");
            codedBody = codedBody.Replace("_", "/");
            byte[] data = Convert.FromBase64String(codedBody);
            body = Encoding.UTF8.GetString(data);
            //        }
            //     }


            return PartialView("~/Views/Gmail/EmailContent.cshtml", body);
        }


        [Authorize]
        public async Task<IActionResult> GoToSendEmail(string emailId = "")
        {

            return View("SendEmailView", emailId);
        }

        [HttpPost]
        [System.Web.Mvc.ValidateInput(false)]
        public async Task<IActionResult> SendEmail(string subject, string toEmail, string body)
        {
            string UserEmail = GetUserEmail();
            var service = GetService();

            var mailMessage = new System.Net.Mail.MailMessage();
            mailMessage.From = new System.Net.Mail.MailAddress(UserEmail);
            mailMessage.To.Add(toEmail);
            mailMessage.ReplyToList.Add(UserEmail);
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = true;
            var mimeMessage = MimeKit.MimeMessage.CreateFromMailMessage(mailMessage);
            var gmailMessage = new Google.Apis.Gmail.v1.Data.Message
            {
                Raw = Encode(mimeMessage.ToString())
            };
            Google.Apis.Gmail.v1.UsersResource.MessagesResource.SendRequest request = service.Users.Messages.Send(gmailMessage, UserEmail);
            request.Execute();
            return View("~/Views/Home/Index.cshtml");
        }
        public string Encode(string text)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(text);

            return System.Convert.ToBase64String(bytes)
                .Replace('+', '-')
                .Replace('/', '_')
                .Replace("=", "");
        }
    }
}
