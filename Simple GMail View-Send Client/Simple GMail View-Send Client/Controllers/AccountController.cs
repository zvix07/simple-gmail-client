﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simple_GMail_View_Send_Client.Controllers
{
    public class AccountController: Controller
    {
        [HttpGet]
        public IActionResult Login(string returnUrl = "/")
        {
          
           return Challenge(new AuthenticationProperties() { RedirectUri = returnUrl }, "Google");
        }
        
        
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Logout()
        {

              await HttpContext.SignOutAsync();


            return RedirectToAction("Index", "Home");
        }


    }
}
