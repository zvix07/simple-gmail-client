﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simple_GMail_View_Send_Client.Models
{
    public class MessageModel
    {
        public int ID { get; set; }
        public string EmailTitle { get; set; }
        public string Date { get; set; }
        public string EmailId { get; set; }
        public string From { get; set; }
    }
}
